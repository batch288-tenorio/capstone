const express = require('express');
const app = express();
const port = 4001;
const mongoose = require('mongoose');
const usersRoutes = require('./routes/usersRoute.js');
const productsRoutes = require('./routes/productsRoutes.js');
const cors = require('cors');


mongoose.connect('mongodb+srv://admin:admin@batch288tenorio.oxntzoo.mongodb.net/NHJHardwareAPI?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

const db = mongoose.connection;

	db.on("error", console.error.bind(console, "Error, can't connect to the db!"));

	db.once("open", () => console.log("We are now connected to the db!"));

// Middlewares:
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use(cors());
app.use('/users', usersRoutes);
app.use('/products', productsRoutes);

app.listen(port, () => {
	console.log(`The server is running at port ${port}`);
})