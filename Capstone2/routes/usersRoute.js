const express = require('express');
const usersControllers = require('../controllers/usersControllers.js');
const auth = require("../auth.js");
const router = express.Router();

// Routes for Register new user
router.post("/register", usersControllers.registerUser);

// Routes for log in user
router.post("/login", usersControllers.loginUser);

router.get("/userDetails", auth.verify, usersControllers.retrieveUserDetails);

// routes to get user profile (Admin only)
router.get('/details', auth.verify, usersControllers.getUserDetails);

// route for Create Oder
router.post('/order', auth.verify, usersControllers.createOrder);

router.post('/checkout', auth.verify, usersControllers.checkout);

// router to retrieve current user

// route to set user as admin
router.patch('/:userId/setAsAdmin', auth.verify, usersControllers.userAsAdmin);

module.exports = router;