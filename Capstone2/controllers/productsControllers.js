const Products = require('../models/Products.js');
const auth = require('../auth.js');

// Create Product (Admin Only)
module.exports.addProduct = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){

		let newProduct = new Products({
			name: request.body.name,
			description: request.body.description,
			price: request.body.price,
			isActive: request.body.isActive
		})

		newProduct.save()
		.then(save => response.send(true))
		.catch(error => response.send(false))
	}
	else{
		return response.send(false)
	}
}

// Retrive all products (Admin Only)
module.exports.getAllProducts = (request, response) => {

	
	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){
		Products.find({})
		.then(result => response.send(result))
		.catch(error => response.send(false));
	}
	else{
		return response.send(false)
	}
}

// Retrive all Active products
module.exports.getActiveProducts = (request, response) => {

	Products.find({isActive: true})
	.then(result => response.send(result))
	.catch(error => response.send(false));
}

// Retrive single product
module.exports.getProduct = (request, response) => {
	const productId = request.params.productId;

	Products.findById(productId)
	.then(result => response.send(result))
	.catch(error => response.send(false));
}

// Update Products (Admin Only)
module.exports.updateProduct = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	const productId = request.params.productId;

	const name = request.body.name;
	const description = request.body.description;
	const price = request.body.price;

	let updatedProduct = {
		name: name,
		description: description,
		price: price
	}

	if(userData.isAdmin){
		Products.findByIdAndUpdate(productId, updatedProduct)
		.then(result => response.send(true))
		.catch(error => response.send(false));
	}
	else{
		return response.send(false)
	}
}

// Archive Products (Admin Only)
module.exports.archiveProduct = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	const productId = request.params.productId;

	let archived = {
		isActive: request.body.isActive
	}

	Products.findByIdAndUpdate(productId, archived)
	.then(result => {
		if(userData.isAdmin){
			if(request.body.isActive === false){
				return response.send(true);
			}
			else{
				return response.send(true);
			}
		}
		else{
			return response.send(false);
		}
	})
	.catch(error=> response.send(false));
}	

// Retrieving archived products
module.exports.getArchivedProducts = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){
		Products.find({isActive: false})
		.then(result => response.send(result))
		.catch(error => response.send(false));
	}
	else{
		return response.send(false)
	}
}

module.exports.archiveProduct = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	const productId = request.params.productId;

	let archived = {
		isActive: false
	}

	Products.findByIdAndUpdate(productId, archived)
	.then(result => {
		if(userData.isAdmin){
			return response.send(true);
		}
		else{
			return response.send(false);
		}
	})
	.catch(error=> response.send(false));
}	

module.exports.activateProduct = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	const productId = request.params.productId;

	let activate = {
		isActive: true
	}

	Products.findByIdAndUpdate(productId, activate)
	.then(result => {
		if(userData.isAdmin){
			return response.send(true);
		}
		else{
			return response.send(false);
		}
	})
	.catch(error=> response.send(false));
}	

