import {Container, Row, Col, Button, Form} from 'react-bootstrap';	
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext.js';
import {Link, useNavigate, Navigate, useParams} from 'react-router-dom';
import Swal2 from 'sweetalert2';

export default function ArchiveProduct(){

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [status, setStatus] = useState('');
	const {productId} = useParams();
	
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
			.then(result => result.json())
			.then(data => {
				
				setStatus(data.isActive);

				setName(data.name);
				setDescription(data.description);
				setPrice(data.price)
				
			})
	}, [])

	
	function archive(event) {

		if(user.admin === true){

			event.preventDefault();

			fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archived`, {
				method: 'PATCH',
				headers: {
					'Authorization': `Bearer ${localStorage.getItem('token')}`,
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					isActive: isActive
				})
			})
			.then(result => result.json())
			.then(data => {
				if(data){
					Swal2.fire({
						title: 'Sucessful',
						icon: 'success',
						text: 'Product Successfully updated'
					})

					navigate('/admin')
				}
				else{
					Swal2.fire({
						title: 'Error',
						icon: 'error',
						text: 'Please check details'
					})
				}
			})
		
		}
	  
		return(
			<Container className = 'mt-5'>
				<Row>
					<Col className = 'col-6 mx-auto'>
						<h1 className = 'text-center'>Update Product Information</h1>
						<Form onSubmit = {event => archive(event)}> 

						
						      <Form.Group className="mb-3 mt-5" controlId="formBasicEmail">
						        <Form.Label>Status</Form.Label>
						        <Form.Control 
						        	type="text" 
						        	value = {status}
						        	onChange = {event => setStatus(event.target.value)}
						        	placeholder="Enter Product Name" />
						      </Form.Group>

						      

						      <Button 
						     	 variant="primary" 
						     	 type="submit"
							    >
						        Submit
						      </Button>
						    </Form>

					</Col>
				</Row>
			</Container>
		
		)
	}
}


