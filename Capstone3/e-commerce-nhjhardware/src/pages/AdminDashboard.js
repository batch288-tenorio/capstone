import {useEffect, useState, useContext} from 'react';
import ProductTable from '../components/ProductTable.js';
import Table from 'react-bootstrap/Table';
import {Container, Row, Col, Button} from 'react-bootstrap';
import {useParams, useNavigate, Link, Navigate} from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal2 from 'sweetalert2';

export default function AdminDashboard(){

	const [products, setProducts] = useState([]);
	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	useEffect(() =>{
		if(user.isAdmin === true){
			fetch(`${process.env.REACT_APP_API_URL}/products/`, {
				method: 'GET',
				headers: {
					'Authorization' : `Bearer ${localStorage.getItem('token')}`,
					'Content-Type' : 'application/json'
				}
			})
			.then(result => result.json())
			.then(data => {
				setProducts(data.map(product => {
					return(
						<ProductTable key = {product._id} productProp = {product} 
						onActivate = {() => {
                            archive(product._id, true) 
                        }} onDisable = {() => {
                            archive(product._id, false)
                        }} />

					)
				}))
			})
		}
	})

	function archive(productId, isActive) {
		

			fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/${isActive ? "activate" : "archive"}`, {
				method: 'PATCH',
				headers: {
					'Authorization': `Bearer ${localStorage.getItem('token')}`,
					'Content-Type': 'application/json'
				}
				
			})
			.then(result => result.json())
			.then(data => {
				console.log(data);
				if(data){
					if(isActive){
						Swal2.fire({
						title: 'Sucessful',
						icon: 'success',
						text: 'Product Successfully Activated'
						})

					}
					else{
						Swal2.fire({
						title: 'Sucessful',
						icon: 'success',
						text: 'Product Successfully Archived'
						})

					}
					
				}
				else{
					Swal2.fire({
						title: 'Error',
						icon: 'error',
						text: 'Please check details'
					})
				}
			})
		
		
	}

	return(
		
		<div className = 'text-center'>
			<h1 className = 'text-center mt-3'>Admin Dashboard</h1>
				
				     <Button className = 'btn1 bg-success'
				     	as = {Link} to = {'/addproduct'}
				     	 variant="secondary" 
				     	 type="submit"
					    >
				        Add Products
				      </Button>
		      
			      <Container className = 'mt-3 props-container bg-dark p-2 rounder-3'>
			      	<Row>

			      		<Col className = 'd-flex align-items-center justify-content-center'>
			      			<h5 className = 'mx-auto text-light'>Name</h5>
			      		</Col>

			      		<Col className = 'd-flex align-items-center justify-content-center'>
			      			<h5 className = 'mx-auto text-light'>Description</h5>
			      		</Col>

			      		<Col className = 'd-flex align-items-center justify-content-center'>
			      			<h5 className = 'mx-auto text-light'>Price</h5>
			      		</Col>

			      		<Col className = 'd-flex align-items-center justify-content-center'>
			      			<h5 className = 'mx-auto text-light'>Status</h5>
			      		</Col>

			      		<Col>
			      			<h5 className = 'mx-auto text-light'>Action</h5>    			
			      		</Col>
			      	</Row>
			      </Container>
			{products}
		</div>
		
	)
}



