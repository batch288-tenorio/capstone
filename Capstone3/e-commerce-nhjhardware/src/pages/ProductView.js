import {Container, Row, Col, Button, Card} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import {useParams, useNavigate} from 'react-router-dom';
import Swal2 from 'sweetalert2';
import Table from 'react-bootstrap/Table';

export default function ProductView(){

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const navigate = useNavigate();
	const { productId } = useParams();


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(result => result.json())
		.then(data => {
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			
		})
	}, [])

	const checkout = (productId) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
			method: 'POST',
			headers: {
				'Authorization' : `Bearer ${localStorage.getItem('token')}`,
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				id: productId
			})
		})
		.then(response => response.json())
		.then(data => {
			if(data){				
				Swal2.fire({
					title: 'Product Checkout',
					icon: 'success',
					text: 'You have Successfully checkout this product'
				})
				navigate('/products');
			}
			else{
				Swal2.fire({
					title: 'No account yet, please register',
					text: 'Please Log in to checkout product'
				})
			}
		})
	}

	return(
		<Container>
			<Row>
				<Col className = 'col-10 mt-3 mx-auto mt-5'>

					<h1 className = 'text-center'>Checkout Order</h1>
					<Card className="text-center cardHighlight mt-3">
					      
					      <Card.Body>
					        <Card.Title>{name}</Card.Title>
					        <Card.Text>{description}</Card.Text>
					          
					        <Card.Text>PHP {price}</Card.Text>
					      </Card.Body>
					   
					   		<Card.Footer className="text-muted"><Button 
					   			variant="primary"
					   			onClick = {() => checkout(productId)}
					   			>Check Out</Button>
					   		</Card.Footer>
					           
					  </Card>        

				</Col>
			</Row>
		</Container>
			
	)
}

