import {Container, Row, Col, Button, Form} from 'react-bootstrap';	
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext.js';
import {Link, useNavigate, Navigate} from 'react-router-dom';
import Swal2 from 'sweetalert2';

export default function AddProduct(){
	
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const {user, setUser} = useContext(UserContext);	
	const navigate = useNavigate();
	const [isDisabled, setIsDisabled] = useState(true);
	
	useEffect(() => {
		if(name !== '' && description !== '' && price !== '' ){
			setIsDisabled(false);
		}
		else{
			setIsDisabled(true);
		}
	}, [name, description, price])

	function addproduct(event){
			
			event.preventDefault()

			fetch(`${process.env.REACT_APP_API_URL}/products/addProducts`, {
				method : 'POST',
				headers: {
					'Authorization' : `Bearer ${localStorage.getItem('token')}`,
					'Content-Type' : 'application/json'
				},
				body: JSON.stringify({
					name: name,
					description: description,
					price: price						
				})
			})
			.then(result=> result.json())
			.then(data => {
				if(data){			
					Swal2.fire({
						title: 'Sucessful',
						icon: 'success',
						text: 'Product already added successfully'
					})
					navigate('/admin')
				}
				else{
					Swal2.fire({
						title: 'Error',
						icon: 'error',
						text: 'Please check details'
					})
				}			
			})	
	}

	return(
		
		<Container className = 'mt-5'>
			<Row>
				<Col className = 'col-6 mx-auto'>
					<h1 className = 'text-center'>Additional Products</h1>
					<Form onSubmit = {event => addproduct(event)}> 
					
					      <Form.Group className="mb-3" controlId="formBasicEmail">
					        <Form.Label>Product Name</Form.Label>
					        <Form.Control 
					        	type="text" 
					        	value = {name}
					        	onChange = {event => setName(event.target.value)}
					        	placeholder="Enter Product Name" />
					      </Form.Group>

					      <Form.Group className="mb-3" controlId="formBasicPassword">
					        <Form.Label>Description</Form.Label>
					        <Form.Control 
					        	type="text" 
					        	value = {description}
					        	onChange = {event => setDescription(event.target.value)}
					        	placeholder="Description" />
					      </Form.Group>

					      <Form.Group className="mb-3" controlId="formBasicPassword">
					        <Form.Label>Price</Form.Label>
					        <Form.Control 
					        	type="number" 
					        	value = {price}
					        	onChange = {event => setPrice(event.target.value)}
					        	placeholder="Price" />
					      </Form.Group>
				      
					      <Button 
					     	 variant="primary" 
					     	 type="submit"
						     disabled = {isDisabled}>
					        Submit
					      </Button>
					    </Form>

				</Col>
			</Row>
		</Container>
	)
}
