
import{Button, Container, Row, Col} from 'react-bootstrap';

import {Link} from 'react-router-dom';

export default function Banner() {

	return(
		<Container>
			<Row>
				<Col className = 'mt-3 text-center'>
					<h1>Welcome to NHJ Hardware</h1>
					<img src="./nhjfamily.jpg" className="App-logo photo" alt="NHJ Family" width="100%" height="100%" />
					
					{/*Footer*/}
					<div className = 'footer2 mt-3'>
						<p className="ms-5">Contact Us</p>
						<p className="ms-5"><a href="https://www.facebook.com/profile.php?id=100064144895979" target= "blank" >Facebook Page</a></p>
						
						
					</div>

				</Col>
			</Row>
		</Container>
	)

}
