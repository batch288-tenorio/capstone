import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import UserContext from '../UserContext';
import {useState, useEffect, useContext} from 'react';
import {Link, useParams} from 'react-router-dom';
import Table from 'react-bootstrap/Table';

export default function ProductTable({onDisable, onActivate, ...props}){

	const {user} = useContext(UserContext);
	const {_id, name, description, price, isActive} = props.productProp;
	const [count, setCount] = useState(0);
	const [isDisabled, setIsDisabled] = useState(false);
	const { productId } = useParams()

	return(

		<Container className = 'mt-3 props-container bg-light p-2 rounder-3'>
			<Row>

				<Col className = 'd-flex align-items-center justify-content-center'>
					<h5 className = 'mx-auto'>{name}</h5>
				</Col>

				<Col className = 'd-flex align-items-center justify-content-center'>
					<p className = 'mx-auto'>{description}</p>
				</Col>

				<Col className = 'd-flex align-items-center justify-content-center'>
					<p className = 'mx-auto'>{price}</p>
				</Col>

				<Col className = 'd-flex align-items-center justify-content-center'>
					<p className = 'mx-auto'>{
                        isActive === true
                        ?
                        'Available'
                        :
                        'Unavailable'
                    }</p>
				</Col>

				<Col>

					{isActive === true ? <Button onClick = {onDisable} className = 'btn2 bg-primary'>Disable</Button> : <Button onClick = {onActivate} className = 'btn2 bg-info'>Activate</Button> }
					
					
					
					<Button as = {Link} to = {`/update/${_id}`} className = 'btn2 bg-danger'>Update</Button>

				</Col>

			</Row>
		</Container>
		
	)
}

