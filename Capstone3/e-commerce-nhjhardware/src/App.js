import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {useState, useEffect} from 'react';
import Home from './pages/Home.js';
import AppNavBar from './components/AppNavBar.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import PageNotFound from './pages/Pagenotfound.js';
import ProductView from './pages/ProductView.js';
import Products from './pages/Products.js';
import AdminDashboard from './pages/AdminDashboard.js';
import AddProduct from './pages/AddProducts.js';
import AdminProductView from './pages/AdminProductView.js';
import ProductTable from './components/ProductTable.js';
import UpdateProduct from './pages/UpdateProduct.js';
import ArchiveProduct from './pages/ArchiveProduct.js';
import {UserProvider} from './UserContext.js';
import {BrowserRouter, Route, Routes} from 'react-router-dom';

function App() {

  let userDetails = {};
    useEffect(()=>{
      if(localStorage.getItem('token') === null){
        userDetails = {
          id: null,
          isAdmin: null
        }
      }else{
          fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(result => result.json())
        .then(data => {
            userDetails = {
                id : data._id,
                isAdmin: data.isAdmin
            };

        })
      }
    }, [])
 
  const [user, setUser] = useState(userDetails);
  const unsetUser = () => {
    localStorage.clear();
  }

  return (
    <UserProvider value = {{user, setUser, unsetUser}}>

        <BrowserRouter>

          <AppNavBar />

          <Routes>
            <Route path = '/' element = {<Home/>} />    
            <Route path = '/register' element = {<Register/>} />
            <Route path = '/login' element = {<Login/>} />
            <Route path = '/logout' element = {<Logout/>} />
            <Route path = '/products' element = {<Products/>} />
            <Route path = '/products/:productId' element = {<ProductView />} />
            <Route path = '/products/:productId' element = {<AdminProductView />} />
            <Route path = '*' element = {<PageNotFound />} />
            <Route path = '/admin' element = {<AdminDashboard />} />
            <Route path = '/addproduct' element = {<AddProduct />} />
            <Route path = '/update/:productId' element = {<UpdateProduct />} />
            <Route path = '/archive/:productId' element = {<ArchiveProduct />} />

          </Routes>
      
        </BrowserRouter>
    </UserProvider>

  );
}

export default App;

